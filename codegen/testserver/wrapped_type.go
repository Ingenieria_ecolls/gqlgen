package testserver

import "gitlab.com/Ingenieria_ecolls/gqlgen/codegen/testserver/otherpkg"

type WrappedScalar = otherpkg.Scalar
type WrappedStruct otherpkg.Struct
type WrappedMap otherpkg.Map
type WrappedSlice otherpkg.Slice
