package main

import (
	"log"
	"net/http"

	"gitlab.com/Ingenieria_ecolls/gqlgen/example/selection"
	"gitlab.com/Ingenieria_ecolls/gqlgen/graphql/handler"
	"gitlab.com/Ingenieria_ecolls/gqlgen/graphql/playground"
)

func main() {
	http.Handle("/", playground.Handler("Selection Demo", "/query"))
	http.Handle("/query", handler.NewDefaultServer(selection.NewExecutableSchema(selection.Config{Resolvers: &selection.Resolver{}})))
	log.Fatal(http.ListenAndServe(":8086", nil))
}
