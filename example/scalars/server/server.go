package main

import (
	"log"
	"net/http"

	"gitlab.com/Ingenieria_ecolls/gqlgen/example/scalars"
	"gitlab.com/Ingenieria_ecolls/gqlgen/graphql/handler"
	"gitlab.com/Ingenieria_ecolls/gqlgen/graphql/playground"
)

func main() {
	http.Handle("/", playground.Handler("Starwars", "/query"))
	http.Handle("/query", handler.NewDefaultServer(scalars.NewExecutableSchema(scalars.Config{Resolvers: &scalars.Resolver{}})))

	log.Fatal(http.ListenAndServe(":8084", nil))
}
