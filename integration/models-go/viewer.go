package models

import "gitlab.com/Ingenieria_ecolls/gqlgen/integration/remote_api"

type Viewer struct {
	User *remote_api.User
}
