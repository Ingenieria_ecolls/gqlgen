module gitlab.com/Ingenieria_ecolls/gqlgen

go 1.12

require (
	gitlab.com/Ingenieria_ecolls/gqlgen v0.13.0
	github.com/go-chi/chi v3.3.2+incompatible
	github.com/gorilla/websocket v1.5.0
	github.com/hashicorp/golang-lru v0.5.0
	github.com/logrusorgru/aurora v0.0.0-20200102142835-e9ef32dff381
	github.com/matryer/moq v0.2.7
	github.com/mattn/go-colorable v0.1.13
	github.com/mattn/go-isatty v0.0.19
	github.com/mitchellh/mapstructure v1.5.0
	github.com/opentracing/opentracing-go v1.0.2
	github.com/pkg/errors v0.8.1
	github.com/rs/cors v1.6.0
	github.com/stretchr/testify v1.8.2
	github.com/urfave/cli/v2 v2.25.5
	github.com/vektah/dataloaden v0.2.1-0.20190515034641-a19b9a6e7c9e
	github.com/vektah/gqlparser/v2 v2.5.10
	golang.org/x/tools v0.9.3
	gopkg.in/yaml.v2 v2.4.0
// sourcegraph.com/sourcegraph/appdash v0.0.0-20180110180208-2cc67fd64755
// sourcegraph.com/sourcegraph/appdash-data v0.0.0-20151005221446-73f23eafcf67 // indirect
)
