package cmd

import (
	"fmt"

	"github.com/urfave/cli/v2"
	"gitlab.com/Ingenieria_ecolls/gqlgen/graphql"
)

var versionCmd = &cli.Command{
	Name:  "version",
	Usage: "print the version string",
	Action: func(ctx *cli.Context) error {
		fmt.Println(graphql.Version)
		return nil
	},
}
